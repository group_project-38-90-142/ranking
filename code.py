
SPACE = " "
def read_file(file_name: str) -> str:
    with open(file_name) as file:
        reading =  file.read()
        return reading
    
# print(read_file("studentRanking.txt"))

def counting_num_of_subjects(file_name:str) -> str:
    content = read_file(file_name)
    line = content.splitlines()
    subjects = line[0].split(SPACE)
    return len(subjects) - 1

# print(counting_num_of_subjects("studentRanking.txt"))

# will check for individual equality
def is_greater(a: int, b: int) -> bool:
	return a > b
		
# checks whether all marks scored by a are greater or vice - versa
def compare_marks(marks_a: list[int],marks_b: list[int]) -> bool:
	individual_marks = zip(marks_a, marks_b)
	check = []
	for a, b in individual_marks:	
		check.append(is_greater(a, b))

	return all(check)



# ranking students
from itertools import combinations
def ranking(*student: list) -> list:
    rank = []
    for x in range(len(combinations(student, 2))):
        A, B = combinations(student, 2)[x][0], combinations(student, 2)[x][1]
        if compare_marks(A, B):
            rank.append('A', 'B')
        elif compare_marks(B, A):
            rank.append('B', 'A')
    return rank
